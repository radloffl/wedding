# **Thank you!**

If you took any photos on our big day (even if we’re not in them), please send an email to [photos@berchradloff.com](mailto:photos@berchradloff.com?subject=Wedding%20Photos) with the photos attached. We’ll be compiling them and adding them to the website this winter. If you just want to be notified when they are posted, send an email to that same address and we’ll let you know!

With thanks and love,

Olivia & Logan

<ul>
<li><a href="mailto:photos@berchradloff.com?subject=Wedding%20Photos" class="button">Send photos</a></li>
<li><a href="mailto:photos@berchradloff.com?subject=Notify%20me!&body=Please%20let%20me%20know%20when%20new%20photos%20are%20posted." class="button">Let me know when photos are posted!</a></li>
</ul>
